<h1>FoodWiz</h1>
<h3>How to start the project</h3>
<hr>
<h5>Create a database</h5>
<ol>
    <li>
        Currently the project files are configured to use a Postgre database
    </li>
    <li>
        Create a database with your desired name
    </li>
</ol>
<hr>
<h5>Start the SpringBoot server</h5>
<ol>
    <li>
        The app is located in the food-wiz-backend folder
    </li>
    <li>
        Navigate to {+ food-wiz-backend/src/main/resources +}
    </li>
    <li>
        Make a copy of {+ application-example.properties +}file and rename the copy to {+ application.properties +}
    </li>
    <li>
        Inside the file set the values that you created for your database
    </li>
    <li>
        The application can now be used in your favorite java IDE
    </li>
</ol>
<hr>
<h5>Flask app</h5>
<ol>
    <li>The app is located in the food-wiz-automation folder</li>
    <li>Create a virtual environment or directly run pip install globally</li>
    <li>Run {+ pip install -r requirements.txt +}</li>
    <li>Run {+ flask run +}</li>
</ol>
<hr>
<h5>React app</h5>
<ol>
    <li>The app is located in the food-wiz-client folder</li>
    <li>Run {+ npm install +}</li>
    <li>Run {+ npm start +}</li>
    <li>Open http://localhost:3000 in your browser</li>
    <li>The project is ready to be used</li>
</ol>
