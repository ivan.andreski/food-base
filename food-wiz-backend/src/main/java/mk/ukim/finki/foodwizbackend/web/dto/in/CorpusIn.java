package mk.ukim.finki.foodwizbackend.web.dto.in;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CorpusIn {
  private String title;
  private String link;
  private String description;
}
