package mk.ukim.finki.foodwizbackend.domain.enumeration;

public enum Status {
    NEW,
    ANNOTATED,
    VALIDATED
}
