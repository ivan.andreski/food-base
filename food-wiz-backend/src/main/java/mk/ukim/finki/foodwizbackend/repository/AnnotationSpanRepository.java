package mk.ukim.finki.foodwizbackend.repository;

import mk.ukim.finki.foodwizbackend.domain.models.AnnotationSpan;
import mk.ukim.finki.foodwizbackend.domain.models.AnnotationSpanDatasetTag;
import mk.ukim.finki.foodwizbackend.domain.models.Document;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AnnotationSpanRepository extends JpaRepository<AnnotationSpan, Long> {

    Optional<AnnotationSpan> findByStartCharAndEndCharAndDocumentAndText(Integer startChar, Integer endChar, Document document, String text);

    @Query(value = "select as2.* " +
            "from annotation_span_dataset_tags asdt " +
            "left join dataset_tags dt on dt.id = asdt.dataset_tag_id " +
            "left join annotation_spans as2 on as2.id = asdt.annotation_span_id " +
            "where asdt.dataset_tag_id = :tag_id " +
            "and as2.document_id = :document_id " +
            "and as2.start_char = :start_char " +
            "and as2.end_char = :end_char ",
            nativeQuery = true
    )
    Optional<AnnotationSpan> findByParams(@Param("start_char") Integer startChar, @Param("end_char") Integer endChar, @Param("document_id") Long documentId, @Param("tag_id") Long tagId);
}
