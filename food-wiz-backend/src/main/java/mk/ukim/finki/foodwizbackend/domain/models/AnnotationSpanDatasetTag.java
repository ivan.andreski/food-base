package mk.ukim.finki.foodwizbackend.domain.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Arrays;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "annotation_span_dataset_tags")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class AnnotationSpanDatasetTag {

    public AnnotationSpanDatasetTag(DatasetTag datasetTag, AnnotationSpan annotationSpan, String tagName, String source) {
        this.datasetTag = datasetTag;
        this.annotationSpan = annotationSpan;
        this.tag = tagName;
        this.source = source;
        this.removed = false;
        this.removedBy = "";
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "annotation_span_id", nullable = false)
    @JsonIgnore
    private AnnotationSpan annotationSpan;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "dataset_tag_id", nullable = false)
    @JsonIgnore
    private DatasetTag datasetTag;

    private String tag;

    private String source;

    // TODO: potential bug with these not getting filled when importing
    private Boolean removed;

    private String removedBy;

    @CreationTimestamp
    @Column(columnDefinition = "TIMESTAMP")
    private LocalDateTime createdAt;

    @UpdateTimestamp
    @Column(columnDefinition = "TIMESTAMP")
    private LocalDateTime modifiedAt;

    public void appendSource(String newSource) {
        if(!this.source.contains(newSource)) {
            this.source += newSource + ",";
        }

        String temp = "";
        for(String s : this.removedBy.split(",")) {
            if(!s.equals(newSource)) {
                temp += s + ",";
            }
        }

        this.removedBy = temp;
    }

    public void removeSource(String newSource) {
        if(!this.removedBy.contains(newSource)) {
            this.removedBy += newSource + ",";
        }

        String temp = "";
        for(String s : this.source.split(",")) {
            if(!s.equals(newSource)) {
                temp += s + ",";
            }
        }

        this.source = temp;
    }

    public AnnotationSpanDatasetTag isTheSame(AnnotationSpan annotationSpan, DatasetTag datasetTag) {
        if(!this.annotationSpan.getStartChar().equals(annotationSpan.getStartChar()))
            return null;
        if(!this.annotationSpan.getEndChar().equals(annotationSpan.getEndChar()))
            return null;
        if(!this.annotationSpan.getText().equals(annotationSpan.getText()))
            return null;
        if(!this.annotationSpan.getDocument().getOriginalId().equals(annotationSpan.getDocument().getOriginalId()))
            return null;
        if(!this.datasetTag.getTagId().equals(datasetTag.getTagId()))
            return null;
        if(!this.datasetTag.getTagName().equals(datasetTag.getTagName()))
            return null;
        if(!this.datasetTag.getDataset().getTitle().equals(datasetTag.getDataset().getTitle()))
            return null;

        return this;
    }
}
