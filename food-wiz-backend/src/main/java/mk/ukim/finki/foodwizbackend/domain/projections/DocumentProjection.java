package mk.ukim.finki.foodwizbackend.domain.projections;

public interface DocumentProjection {

    Long getId();
    String getStatus();
    String getOriginalId();
}
