package mk.ukim.finki.foodwizbackend.repository;

import mk.ukim.finki.foodwizbackend.domain.models.AnnotationSpan;
import mk.ukim.finki.foodwizbackend.domain.models.AnnotationSpanDatasetTag;
import mk.ukim.finki.foodwizbackend.domain.models.DatasetTag;
import mk.ukim.finki.foodwizbackend.domain.models.Document;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AnnotationSpanDatasetTagRepository extends JpaRepository<AnnotationSpanDatasetTag, Long> {

    @Query(value = "select distinct(ad.source) from AnnotationSpanDatasetTag ad")
    List<String> findAllSources();

    @Query(value = "select asdt.* " +
            " from annotation_span_dataset_tags asdt" +
            " left join dataset_tags dt on dt.id = asdt.dataset_tag_id " +
            " left join annotation_spans as2 on as2.id = asdt.annotation_span_id " +
            " where as2.document_id = :document_id",
            nativeQuery = true
    )
    List<AnnotationSpanDatasetTag> getByDocument(@Param("document_id") Long documentId);

    @Query(value = "select asdt.* " +
            " from annotation_span_dataset_tags asdt" +
            " left join dataset_tags dt on dt.id = asdt.dataset_tag_id " +
            " left join annotation_spans as2 on as2.id = asdt.annotation_span_id " +
            " where as2.document_id = :document_id " +
            " and (:sources = '' or :sources like concat('%', asdt.source, '%') )" +
            " and (:datasets = '' or :datasets like concat('%', dt.dataset_id , '%') )",
            nativeQuery = true
    )
    List<AnnotationSpanDatasetTag> getByDocumentAndDatasetAndSource(@Param("document_id") Long documentId, @Param("datasets") String datasets, @Param("sources") String sources);

    @Query(value = "select asdt.* " +
            " from annotation_span_dataset_tags asdt" +
            " left join dataset_tags dt on dt.id = asdt.dataset_tag_id " +
            " left join annotation_spans as2 on as2.id = asdt.annotation_span_id " +
            " where as2.document_id = :document_id " +
            " and (:datasets = '' or :datasets like concat('%', dt.dataset_id , '%') )",
            nativeQuery = true
    )
    List<AnnotationSpanDatasetTag> getByDocumentAndDataset(@Param("document_id") Long documentId, @Param("datasets") String datasets);

    Optional<AnnotationSpanDatasetTag> findByAnnotationSpanAndDatasetTag(AnnotationSpan annotationSpan, DatasetTag datasetTag);

    @Query(value = "SELECT asdt.* " +
            " FROM annotation_span_dataset_tags asdt " +
            " LEFT JOIN dataset_tags dt ON dt.id = asdt.dataset_tag_id " +
            " WHERE dt.dataset_id IN :dataset_ids ", nativeQuery = true)
    List<AnnotationSpanDatasetTag> getByDatasets(@Param("dataset_ids") List<Long> datasetIds);

    @Query("SELECT COUNT(ast) FROM AnnotationSpanDatasetTag ast WHERE ast.source LIKE %:originalSource% AND ast.source LIKE %:otherSource%")
    Long countRecordsWithBothSources(@Param("originalSource") String originalSource, @Param("otherSource") String otherSource);

    @Query("SELECT COUNT(ast) FROM AnnotationSpanDatasetTag ast WHERE ast.source LIKE %:originalSource% AND ast.source NOT LIKE %:otherSource%")
    Long countRecordsWithOriginalSourceOnly(@Param("originalSource") String originalSource, @Param("otherSource") String otherSource);

    @Query("SELECT COUNT(ast) FROM AnnotationSpanDatasetTag ast WHERE ast.source LIKE %:otherSource% AND ast.source NOT LIKE %:originalSource%")
    Long countRecordsWithOtherSourceOnly(@Param("originalSource") String originalSource, @Param("otherSource") String otherSource);

    @Query("SELECT COUNT(ast) FROM AnnotationSpanDatasetTag ast WHERE ast.source NOT LIKE %:originalSource% AND ast.source NOT LIKE %:otherSource%")
    Long countRecordsWithNeitherSource(@Param("originalSource") String originalSource, @Param("otherSource") String otherSource);
}
