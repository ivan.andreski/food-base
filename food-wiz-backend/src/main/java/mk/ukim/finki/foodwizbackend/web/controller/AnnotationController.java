package mk.ukim.finki.foodwizbackend.web.controller;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import mk.ukim.finki.foodwizbackend.domain.dto.out.DocumentResponseDto;
import mk.ukim.finki.foodwizbackend.domain.exceptions.DocumentNotFoundException;
import mk.ukim.finki.foodwizbackend.domain.models.AnnotationSpan;
import mk.ukim.finki.foodwizbackend.domain.models.AnnotationSpanDatasetTag;
import mk.ukim.finki.foodwizbackend.domain.models.Dataset;
import mk.ukim.finki.foodwizbackend.domain.models.Document;
import mk.ukim.finki.foodwizbackend.repository.AnnotationSpanDatasetTagRepository;
import mk.ukim.finki.foodwizbackend.repository.AnnotationSpanRepository;
import mk.ukim.finki.foodwizbackend.repository.DatasetRepository;
import mk.ukim.finki.foodwizbackend.repository.DocumentRepository;
import mk.ukim.finki.foodwizbackend.web.dto.in.AnnotateRequest;
import mk.ukim.finki.foodwizbackend.web.dto.in.AnnotationSpanDatasetTagDto;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/annotate")
@CrossOrigin
public class AnnotationController {

    private DocumentRepository documentRepository;
    private DatasetRepository datasetRepository;
    private AnnotationSpanRepository annotationSpanRepository;
    private AnnotationSpanDatasetTagRepository annotationSpanDatasetTagRepository;

    public AnnotationController(DocumentRepository documentRepository, DatasetRepository datasetRepository, AnnotationSpanRepository annotationSpanRepository, AnnotationSpanDatasetTagRepository annotationSpanDatasetTagRepository) {
        this.documentRepository = documentRepository;
        this.datasetRepository = datasetRepository;
        this.annotationSpanRepository = annotationSpanRepository;
        this.annotationSpanDatasetTagRepository = annotationSpanDatasetTagRepository;
    }

    @PostMapping("/{id}")
    public boolean annotateDocumentWithDataset(@PathVariable Long id, @RequestBody AnnotateRequest annotateRequest) {
        Document document = documentRepository.findById(id).orElseThrow(() -> new DocumentNotFoundException(id));
        List<Dataset> datasets = annotateRequest
                .getDatasetNames()
                .stream()
                .map((dataset) -> datasetRepository.findByTitle(dataset))
                .toList();

        List<Word> words = splitIntoWords(document.getText());
        List<AnnotationSpanDatasetTag> annotationSpanDatasetTags = annotationSpanDatasetTagRepository.getByDatasets(datasets.stream().map(Dataset::getId).toList());

        List<AnnotationSpan> newSpans = new ArrayList<>();
        List<AnnotationSpanDatasetTag> newTags = new ArrayList<>();

        for (Word word : words) {
            for (AnnotationSpanDatasetTag annotationSpanDatasetTag : annotationSpanDatasetTags) {
                if (word.getValue().toLowerCase().strip().equals(annotationSpanDatasetTag.getAnnotationSpan().getText().toLowerCase().strip())) {
                    AnnotationSpan annotationSpan = null;
                    for (AnnotationSpan as : newSpans) {
                        if (as.getStartChar().equals(word.getStart())
                                && as.getEndChar().equals(word.getEnd())) {
                            annotationSpan = as;
                            break;
                        }
                    }
                    if (annotationSpan == null) {
                        annotationSpan = new AnnotationSpan(word.getStart(), word.getEnd(), word.getValue(), document);
                        annotationSpan = annotationSpanRepository.save(annotationSpan);
                        newSpans.add(annotationSpan);
                    }

                    AnnotationSpanDatasetTag newAnnotationSpanDatasetTag = new AnnotationSpanDatasetTag(
                            annotationSpanDatasetTag.getDatasetTag(),
                            annotationSpan,
                            annotationSpanDatasetTag.getDatasetTag().getTagName(),
                            "LOCAL_MODEL");
                    newTags.add(newAnnotationSpanDatasetTag);
                }
            }
        }

        annotationSpanDatasetTagRepository.saveAll(newTags);

        return true;
    }

    public List<Word> splitIntoWords(String text) {
        List<Word> words = new ArrayList<>();
        String[] tokens = text.split("\\s+"); // Split by whitespace

        int currentPosition = 0;
        for (String token : tokens) {
            int start = text.indexOf(token, currentPosition);
            int end = start + token.length();
            currentPosition = end + 1;
            words.add(new Word(token, start, end));
        }

        return words;
    }
}

@Getter
@AllArgsConstructor
class Word {
    private final String value;
    private final int start;
    private final int end;
}
