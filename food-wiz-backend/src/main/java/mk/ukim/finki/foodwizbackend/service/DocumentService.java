package mk.ukim.finki.foodwizbackend.service;

import mk.ukim.finki.foodwizbackend.domain.dto.out.DocumentResponseDto;
import mk.ukim.finki.foodwizbackend.domain.models.Document;

public interface DocumentService {

    DocumentResponseDto get(Long id);

    Document validate(Long id);


}
