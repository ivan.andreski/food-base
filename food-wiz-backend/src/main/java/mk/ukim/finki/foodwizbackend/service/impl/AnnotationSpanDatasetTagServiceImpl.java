package mk.ukim.finki.foodwizbackend.service.impl;

import mk.ukim.finki.foodwizbackend.domain.exceptions.DocumentNotFoundException;
import mk.ukim.finki.foodwizbackend.domain.models.AnnotationSpan;
import mk.ukim.finki.foodwizbackend.domain.models.AnnotationSpanDatasetTag;
import mk.ukim.finki.foodwizbackend.domain.models.DatasetTag;
import mk.ukim.finki.foodwizbackend.domain.models.Document;
import mk.ukim.finki.foodwizbackend.repository.AnnotationSpanDatasetTagRepository;
import mk.ukim.finki.foodwizbackend.repository.AnnotationSpanRepository;
import mk.ukim.finki.foodwizbackend.repository.DatasetTagRepository;
import mk.ukim.finki.foodwizbackend.repository.DocumentRepository;
import mk.ukim.finki.foodwizbackend.service.AnnotationSpanDatasetTagService;
import mk.ukim.finki.foodwizbackend.web.dto.in.AnnotationSpanDatasetTagDto;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class AnnotationSpanDatasetTagServiceImpl implements AnnotationSpanDatasetTagService {

    private final AnnotationSpanDatasetTagRepository annotationSpanDatasetTagRepository;
    private final AnnotationSpanRepository annotationSpanRepository;
    private final DatasetTagRepository datasetTagRepository;
    private final DocumentRepository documentRepository;

    public AnnotationSpanDatasetTagServiceImpl(AnnotationSpanDatasetTagRepository annotationSpanDatasetTagRepository, AnnotationSpanRepository annotationSpanRepository, DatasetTagRepository datasetTagRepository, DocumentRepository documentRepository) {
        this.annotationSpanDatasetTagRepository = annotationSpanDatasetTagRepository;
        this.annotationSpanRepository = annotationSpanRepository;
        this.datasetTagRepository = datasetTagRepository;
        this.documentRepository = documentRepository;
    }

    @Override
    public List<String> getAllSources() {
        Set<String> distinctSources = new HashSet<>();
        annotationSpanDatasetTagRepository.findAllSources()
                .forEach(source -> {
                    Arrays.stream(source.split(",")).forEach(sourceSplit -> {
                        if (!sourceSplit.isEmpty()) {
                            distinctSources.add(sourceSplit);
                        }
                    });
                });
        return distinctSources.stream().toList();
    }

    @Override
    public AnnotationSpanDatasetTag addTag(AnnotationSpanDatasetTagDto dto, Long tagId, String username) {
        Document document = documentRepository
                .findById(dto.getDocumentId())
                .orElseThrow(() -> new DocumentNotFoundException(dto.getDocumentId()));
        AnnotationSpan annotationSpan = annotationSpanRepository
                .findByParams(dto.getStartChar(), dto.getEndChar(), document.getId(), tagId)
                .orElse(null);
        DatasetTag datasetTag = datasetTagRepository
                .findById(tagId)
                .orElseThrow(() -> new RuntimeException("Dataset tag not found"));
        AnnotationSpanDatasetTag annotationSpanDatasetTag = annotationSpanDatasetTagRepository
                .findByAnnotationSpanAndDatasetTag(annotationSpan, datasetTag)
                .orElse(null);

        if (annotationSpanDatasetTag != null) {
            annotationSpanDatasetTag.setRemoved(false);
            annotationSpanDatasetTag.appendSource(username);
            return annotationSpanDatasetTagRepository.save(annotationSpanDatasetTag);
        }
        if (annotationSpan == null) {
            annotationSpan = new AnnotationSpan(dto.getStartChar(), dto.getEndChar(), dto.getText(), document);
            annotationSpan = annotationSpanRepository.save(annotationSpan);
        }

        annotationSpanDatasetTag = new AnnotationSpanDatasetTag(datasetTag, annotationSpan, datasetTag.getTagName(), username);
        return annotationSpanDatasetTagRepository.save(annotationSpanDatasetTag);
    }

    @Override
    public AnnotationSpanDatasetTag markDeleteTag(AnnotationSpanDatasetTagDto dto, String tagId, String username) {
        Document document = documentRepository
                .findById(dto.getDocumentId())
                .orElseThrow(() -> new DocumentNotFoundException(dto.getDocumentId()));
        DatasetTag datasetTag = datasetTagRepository
                .findByTagId(tagId)
                .orElseThrow(() -> new RuntimeException("Dataset tag not found"));
        AnnotationSpan annotationSpan = annotationSpanRepository
                .findByParams(dto.getStartChar(), dto.getEndChar(), document.getId(), datasetTag.getId())
                .orElseThrow(() -> new RuntimeException("Annotation Span not found"));
        AnnotationSpanDatasetTag annotationSpanDatasetTag = annotationSpanDatasetTagRepository
                .findByAnnotationSpanAndDatasetTag(annotationSpan, datasetTag)
                .orElseThrow(() -> new RuntimeException("Annotation Span DatasetTag not found"));

        annotationSpanDatasetTag.setRemoved(true);
        annotationSpanDatasetTag.removeSource(username);

        return annotationSpanDatasetTagRepository.save(annotationSpanDatasetTag);
    }

    @Override
    public AnnotationSpanDatasetTag voteRemove(AnnotationSpanDatasetTagDto dto, String tagId, String username) {
        Document document = documentRepository
                .findById(dto.getDocumentId())
                .orElseThrow(() -> new DocumentNotFoundException(dto.getDocumentId()));
        DatasetTag datasetTag = datasetTagRepository
                .findByTagId(tagId)
                .orElseThrow(() -> new RuntimeException("Dataset tag not found"));
        AnnotationSpan annotationSpan = annotationSpanRepository
                .findByParams(dto.getStartChar(), dto.getEndChar(), document.getId(), datasetTag.getId())
                .orElseThrow(() -> new RuntimeException("Annotation Span not found"));
        AnnotationSpanDatasetTag annotationSpanDatasetTag = annotationSpanDatasetTagRepository
                .findByAnnotationSpanAndDatasetTag(annotationSpan, datasetTag)
                .orElseThrow(() -> new RuntimeException("Annotation Span DatasetTag not found"));

        annotationSpanDatasetTag.removeSource(username);

        return annotationSpanDatasetTagRepository.save(annotationSpanDatasetTag);
    }

    @Override
    public String calculatePrecision(String originalSource, String otherSource) {
        Long tp = annotationSpanDatasetTagRepository.countRecordsWithBothSources(originalSource, otherSource);
        Long tn = annotationSpanDatasetTagRepository.countRecordsWithOriginalSourceOnly(originalSource, otherSource);
        Long fp = annotationSpanDatasetTagRepository.countRecordsWithOtherSourceOnly(originalSource, otherSource);
        Long fn = annotationSpanDatasetTagRepository.countRecordsWithNeitherSource(originalSource, otherSource);

        double precision = tp.doubleValue() / (tp.doubleValue() + fp.doubleValue());
        double recall = tp.doubleValue() / (tp.doubleValue() + fn.doubleValue());

        if (precision + recall == 0) {
            return "0";
        }
        double f1Score = 2 * (precision * recall) / (precision + recall);

        return String.valueOf(f1Score);
    }


}
