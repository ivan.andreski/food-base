package mk.ukim.finki.foodwizbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FoodWizBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(FoodWizBackendApplication.class, args);
	}

}
