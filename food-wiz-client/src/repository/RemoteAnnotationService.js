import axios from "axios";

const RemoteAnnotationService = {
  localAnnotate: (document, dataset) => {
    const datasets = [];
    Object.keys(dataset).forEach((key) => {
      if(dataset[key]) {
        datasets.push(key);
      }
    });

    const request = {
      datasetNames: datasets,
    };

    return axios.post(
      `${process.env.REACT_APP_BACKEND_URL}/annotate/${document.id}`,
      request
    );
  },

  remoteAnnotate: (document, dataset) => {
    const request = {
      text: document.text,
      dataset_name: dataset,
    };

    return axios.post(
      `${process.env.REACT_APP_ANNOTATE_URL}/food/predict`,
      request
    );
  },

  remoteFineTune: (document, dataset, tags) => {
    const tempTags = [];
    tags.forEach((tag) => {
      const t = tag;
      t.tags = tag.tags.filter((t) => t.dataset === dataset);
      tempTags.push(t);
    });
    const requestTags = [];
    tempTags.forEach((tag) => {
      if (tag.tags.length > 0) {
        tag.tags.forEach((t) => {
          requestTags.push({
            od: tag.start,
            do: tag.end,
            word: tag.text,
            tag: t.token,
          });
        });
      }
    });

    const request = {
      text: document.text,
      dataset_name: dataset,
      tags: requestTags,
    };

    return axios.post(
      `${process.env.REACT_APP_ANNOTATE_URL}/food/finetune`,
      request
    );
  },
};

export default RemoteAnnotationService;
