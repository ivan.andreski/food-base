import axios from "../custom-axios/axios";

const UploadService = {
  uploadJson: (type, data) => {
    return axios.post(`${type.toLowerCase()}/convert`, data);
  },
  uploadRDF: (data) => {
    return axios.post('dataset/convertRdf', data)
  }
};

export default UploadService;
