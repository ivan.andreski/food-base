import axios from "../custom-axios/axios";

const DatasetTagService = {
  fetchDatasetTags: (datasets) => {
    return axios.get(`/datasetTag`);
  },

  addTag: (tagId, formData) => {
    return axios.put(`/annotationSpanDatasetTag/${tagId}`, formData);
  },

  voteRemove: (tagId, formData) => {
    return axios.put(`/annotationSpanDatasetTag/voteRemove/${tagId}`, formData);
  },

  markDelete: (tagId, formData) => {
    return axios.put(`/annotationSpanDatasetTag/markDelete/${tagId}`, formData);
  },
};

export default DatasetTagService;
