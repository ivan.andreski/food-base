import axios from "../custom-axios/axios";

const DocumentService = {
  fetchDocument: (id) => {
    return axios.get(
      `document/${id}`
    );
  },

  validateDocument: (documentId) => {
    return axios.put(`/document/${documentId}/validate`);
  },

  fetchSources: () => {
    return axios.get(`document/sources`);
  },

  calculatePrecision: (originalSource, otherSource) => {
    return axios.get(
      `document/calculate-precision/?originalSource=${originalSource}&otherSource=${otherSource}`
    );
  }
};

export default DocumentService;
