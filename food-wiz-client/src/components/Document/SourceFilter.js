import React from "react";

const SourceFilter = ({ sources, checkedSources, setCheckedSources }) => {
  const handleChange = (e) => {
    const source = e.target.id;
    const value = e.target.checked;

    setCheckedSources((checkedSources) => ({
      ...checkedSources,
      [source]: value,
    }));
  };

  const renderSelects = () => {
    return sources.map((source, i) => (
      <div key={i} className="mb-2">
        <label htmlFor={source}>
          <input
            id={source}
            className="mr-1"
            type="checkbox"
            checked={checkedSources[source]}
            onChange={handleChange}
          />
          <span className=""> {source}</span>
        </label>
      </div>
    ));
  };

  return (
    <div className="col-12">
      <div className="mb-2">Active Annotation Sources:</div>
      {renderSelects()}
      <hr />
    </div>
  );
};

export default SourceFilter;
