import React, { useState } from "react";
import Swal from "sweetalert2";
import { useNavigate } from "react-router-dom";
import Modal from "react-bootstrap/Modal";

import AddTagSelector from "./AddTagSelector";
import DatasetTagService from "../../repository/DatasetTagService";
import PollingChart from "./PollingChart";

const TextModalDialog = ({
  token,
  setSelectedToken,
  datasetTags,
  documentId,
  datasets,
}) => {
  const navigate = useNavigate();
  const [activeTag, setActiveTag] = useState(token?.tags[0]);

  const handleAdd = (tagId) => {
    if (localStorage.getItem("token") === null) {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "You must be logged in for this feature",
      });
    } else {
      let formData = {
        startChar: token.start,
        endChar: token.end,
        tagId: tagId,
        text: token.text,
        documentId: documentId,
      };

      DatasetTagService.addTag(tagId, formData)
        .then((response) => {
          navigate(0);
        })
        .catch((error) => console.log(error));
    }
  };

  const handleVoteRemove = (tagId) => {
    if (localStorage.getItem("token") === null) {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "You must be logged in for this feature",
      });
    } else {
      let formData = {
        startChar: token.start,
        endChar: token.end,
        tagId: tagId,
        text: token.text,
        documentId: documentId,
      };

      DatasetTagService.voteRemove(tagId, formData)
        .then((response) => {
          navigate(0);
        })
        .catch((error) => console.log(error));
    }
  };

  const handleDelete = (tagId) => {
    if (localStorage.getItem("token") === null) {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "You must be logged in for this feature",
      });
    } else {
      let formData = {
        startChar: token.start,
        endChar: token.end,
        tagId: tagId,
        text: token.text,
        documentId: documentId,
      };

      DatasetTagService.markDelete(tagId, formData)
        .then((response) => {
          navigate(0);
        })
        .catch((error) => console.log(error));
    }
  };

  return token == null ? (
    <></>
  ) : (
    <>
      <Modal
        show={token != null}
        onHide={() => setSelectedToken(null)}
        dialogClassName="custom-modal"
      >
        <Modal.Header closeButton>
          <Modal.Title>{token.text}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="row">
            <div className={`col-md-${activeTag ? "6  border-end" : "12"}`}>
              <div className="p-1">
                <b>Datasets:</b>
                <div className="row-12 mb-1 " style={{ width: "100%" }}>
                  {token.tags.map((tag, index) => (
                    <div
                      key={index}
                      onClick={() => setActiveTag(tag)}
                      className={
                        tag.removed === "true"
                          ? "badge m-1 bg-danger"
                          : "badge m-1 bg-secondary"
                      }
                      title={`Dataset: ${tag.dataset}, Source: ${tag.source}, Removed by: ${tag.removedBy}`}
                    >
                      [{tag.link}] {tag.token}
                      <button
                        className="btn btn-dark fw-bold"
                        onClick={() => handleDelete(tag.link, tag)}
                      >
                        X
                      </button>
                    </div>
                  ))}
                </div>
              </div>
            </div>
            {activeTag && (
              <div className="col-md-6">
                <PollingChart
                  tag={activeTag}
                  datasetTags={datasetTags}
                  handleAdd={handleAdd}
                  handleVoteRemove={handleVoteRemove}
                />
              </div>
            )}
          </div>
          <div className="p-2">
            {Object.keys(datasetTags).map((key) => (
              <AddTagSelector
                datasets={datasets}
                datasetTags={datasetTags}
                k={key}
                key={key}
                handleAdd={handleAdd}
              />
            ))}
          </div>
        </Modal.Body>
      </Modal>
    </>
  );
};

export default TextModalDialog;
