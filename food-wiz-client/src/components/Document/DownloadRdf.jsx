import React, { useState, useRef } from "react";

const DownloadRdf = ({ document, tokens }) => {
  const downloadRef = useRef(null);

  const handleRdfDownload = async () => {
    const filteredTokens = tokens.filter((token) => token.tags.length > 0);

    const jsonLdArray = filteredTokens.map((obj) => {
      return {
        "@context": {
          "@vocab": "http://schema.org/",
        },
        "@type": "Text",
        text: obj.text,
        tags: obj.tags.map((tag) => ({
          "@type": "Tag",
          dataset: tag.dataset,
          link: tag.link,
          removed: tag.removed,
          removedBy: tag.removedBy,
          source: tag.source,
          token: tag.token,
        })),
        start: obj.start,
        end: obj.end,
      };
    });

    const jsonLdData = JSON.stringify(jsonLdArray, null, 4);

    const blob = new Blob([jsonLdData], { type: "application/json" });
    const url = URL.createObjectURL(blob);

    downloadRef.current.href = url;
    downloadRef.current.click();

    URL.revokeObjectURL(url);
  };

  return (
    <div className="mb-2">
      <button
        className="btn btn-secondary mt-2 w-100"
        onClick={handleRdfDownload}
      >
        Download JSON-LD
      </button>

      <a
        style={{ display: "none" }}
        download={`${document.id}_rdf.jsonld`}
        href=""
        ref={downloadRef}
      />
    </div>
  );
};

export default DownloadRdf;
