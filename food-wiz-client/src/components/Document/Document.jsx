import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

import DocumentService from "../../repository/DocumentService";
import DatasetService from "../../repository/DatasetService";
import DatasetsFilter from "./DatasetsFilter";
import DatasetTagService from "../../repository/DatasetTagService";
import SourceFilter from "./SourceFilter";
import Annotate from "./Annotate";
import TextModalDialog from "./TextModalDialog";
import Token from "./Token";

import "./style.css";
import DownloadRdf from "./DownloadRdf";
import LoadingAnimation from "../LoadingAnimation/LoadingAnimation";

const Document = () => {
  const { id } = useParams();

  const [loading, setLoading] = useState(false);

  const [document, setDocument] = useState({});
  const [tokens, setTokens] = useState([]);
  const [datasetTags, setDatasetTags] = useState({});
  const [selectedToken, setSelectedToken] = useState(null);

  // filters
  const [datasets, setDatasets] = useState([]);
  const [checkedDatasets, setCheckedDatasets] = useState({});
  const [sources, setSources] = useState([]);
  const [checkedSources, setCheckedSources] = useState({});

  useEffect(() => {
    const fetchDatasets = () => {
      DatasetService.fetchByCorpusId(id).then((resp) => {
        setDatasets(resp.data.datasets);

        const temp = resp.data.datasets.reduce((acc, dataset) => {
          acc[dataset.title] = true;
          return acc;
        }, {});
        setCheckedDatasets(temp);
      });
    };

    const fetchSources = () => {
      DocumentService.fetchSources().then((resp) => {
        setSources(resp.data);

        const temp = resp.data.reduce((acc, source) => {
          acc[source] = true;
          return acc;
        }, {});
        setCheckedSources(temp);
      });
    };

    const fetchDatasetTags = () => {
      DatasetTagService.fetchDatasetTags()
        .then((response) => {
          setDatasetTags(response.data);
        })
        .catch((error) => console.log(error));
    };

    const fetchDocument = () => {
      DocumentService.fetchDocument(id)
        .then((resp) => {
          setDocument(resp.data.document);
          setTokens(JSON.parse(resp.data.tokens));
        })
        .finally(() => setLoading(false));
    };

    setLoading(true);

    fetchDatasets();
    fetchSources();
    fetchDatasetTags();
    fetchDocument();
  }, [id]);

  const getTokens = () => {
    const result = [];
    tokens.forEach((token) => {
      const tags = [];

      token.tags.forEach((tag) => {
        const sourceValues = tag.source.split(",");
        const datasetValues = tag.dataset.split(",");

        if (
          sourceValues.some((value) => checkedSources[value]) &&
          datasetValues.some((value) => checkedDatasets[value])
        ) {
          tags.push(tag);
        }
      });

      result.push({ ...token, tags: tags });
    });

    return result;
  };

  const handleTokenChange = (token) => {
    setSelectedToken(token);
  };

  return (
    <div className="container">
      <div className="row">
        <div className="col-md-10">
          <h1>Document: {id}</h1>
        </div>
        <hr />
        <div className="col-md-3 border-end">
          <DatasetsFilter
            datasets={datasets}
            checkedDatasets={checkedDatasets}
            setCheckedDatasets={setCheckedDatasets}
          />
          <SourceFilter
            sources={sources}
            checkedSources={checkedSources}
            setCheckedSources={setCheckedSources}
          />
        </div>
        <div className="col-md-6">
          {loading ? (
            <LoadingAnimation />
          ) : (
            <div style={{ textAlign: "left" }}>
              {getTokens().map((token, index) => (
                <Token
                  key={index}
                  token={token}
                  handleTokenClick={handleTokenChange}
                />
              ))}
            </div>
          )}
        </div>
        <div className="col-md-3 border-start">
          <DownloadRdf document={document} tokens={getTokens()} />
          <Annotate
            document={document}
            tags={tokens}
            checkedDatasets={checkedDatasets}
            sources={sources}
          />
        </div>
      </div>
      <TextModalDialog
        token={selectedToken}
        datasetTags={datasetTags}
        documentId={id}
        datasets={datasets}
        setSelectedToken={handleTokenChange}
      />
    </div>
  );
};

export default Document;
