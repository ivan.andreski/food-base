import React from "react";

const DatasetsFilter = ({ datasets, checkedDatasets, setCheckedDatasets }) => {
  const handleChange = (e) => {
    const datasetId = e.target.id;
    const value = e.target.checked;

    setCheckedDatasets((checkedDatasets) => ({
      ...checkedDatasets,
      [datasetId]: value,
    }));
  };

  const renderSelects = () => {
    return datasets.map((dataset, i) => (
      <div key={i} className="mb-2">
        <label htmlFor={dataset.title}>
          <input
            id={dataset.title}
            className="mr-1"
            type="checkbox"
            checked={checkedDatasets[dataset.title]}
            onChange={handleChange}
          />
          <span className="text-capitalize"> {dataset.title}</span>
        </label>
      </div>
    ));
  };

  return (
    <div className="col-12">
      <div className="mb-2">Active Annotation Datasets:</div>
      {renderSelects()}
      <hr />
    </div>
  );
};

export default DatasetsFilter;
