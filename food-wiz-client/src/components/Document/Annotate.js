import React, { useEffect, useState } from "react";
import Swal from "sweetalert2";

import LoadingAnimation from "../LoadingAnimation/LoadingAnimation";
import RemoteAnnotationService from "../../repository/RemoteAnnotationService";
import DocumentService from "../../repository/DocumentService";

const Annotate = ({ document, tags, checkedDatasets, sources }) => {
  const [isLoading, setLoading] = useState(false);
  const [model, setModel] = useState("local");
  const [originalSource, setOriginalSource] = useState("");
  const [otherSource, setOtherSource] = useState("");
  const options = sources.map((option) => {
    return <option key={option} value={option}>{option}</option>
  })


  useEffect(() => {
    setOriginalSource(sources === [] ? "" : sources[0]);
    setOtherSource(sources === [] ? "" : sources[0]);
  }, [sources]);

  const handleAnnotateClick = () => {
    setLoading(true);

    if (model === "local") {
      RemoteAnnotationService.localAnnotate(document, checkedDatasets)
        .then((response) => {
          console.log(response.data);
        })
        .catch((e) => console.log(e))
        .finally(() => setLoading(false));
    } else if (model === "remote") {
      RemoteAnnotationService.remoteAnnotate(document, checkedDatasets)
        .then((response) => {
          console.log(response.data);
        })
        .catch((e) => console.log(e))
        .finally(() => setLoading(false));
    }
  };

  const handleFineTuneClick = () => {
    setLoading(true);
    RemoteAnnotationService.remoteFineTune(document, checkedDatasets, tags)
      .then((response) => {
        console.log(response.data);
        Swal.fire({
          icon: "success",
          title: "Success",
          html:
            '<pre style="text-align: left; white-space: pre-line;">' +
            response.data.status +
            "</pre>",
        });
      })
      .catch((e) => console.log(e))
      .finally(() => setLoading(false));
  };

  const handleF1ScoreClick = () => {
    setLoading(true);
    DocumentService.calculatePrecision(originalSource, otherSource)
      .then((response) => {
        console.log(response.data);
        Swal.fire({
          icon: "success",
          title: "The F1 score is",
          html:
            '<pre style="text-align: center; white-space: pre-line;">' +
            response.data +
            "</pre>",
        });
      })
      .catch((e) => console.log(e))
      .finally(() => setLoading(false));
  };

  const isDiabled = () => {
    return (
      Object.keys(checkedDatasets).filter((key) => checkedDatasets[key])
        .length === 0
    );
  };

  return isLoading ? (
    <LoadingAnimation />
  ) : (
    <div>
      <button
        className="btn btn-primary mb-2 w-100"
        onClick={handleAnnotateClick}
        disabled={isDiabled()}
      >
        Annotate
      </button>
      <button
        className="btn btn-secondary mb-2 w-100"
        onClick={handleFineTuneClick}
        disabled={isDiabled()}
      >
        Fine Tune
      </button>
      <select
        value={model}
        onChange={(e) => setModel(e.target.value)}
        className="form-select"
      >
        <option value="local">Local model</option>
        <option value="remote">Remote model</option>
      </select>
      <br/>
      F1 score calculation
      <br/><br/>
      Original source
      <select
          className="form-select"
          value={originalSource}
          onChange={(s) => setOriginalSource(s.target.value)}
      >
        {options}
      </select>
      <br/>
      Other source
      <select
          className="form-select"
          value={otherSource}
          onChange={(s) => setOtherSource(s.target.value)}
      >
        {options}
      </select>
      <br/>
      <button
        className="btn btn-primary mb-2 w-100"
        onClick={handleF1ScoreClick}
        disabled={isDiabled()}
      >
        Check F1 score
      </button>
    </div>
  );
};

export default Annotate;
