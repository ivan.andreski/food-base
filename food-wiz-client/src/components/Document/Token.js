import React from "react";

const Token = ({ token, handleTokenClick }) => {
  const getClass = () => {
    return token.tags.length > 0 ? " badge text-bg-success" : "";
  };

  return (
    <>
      <span
        className={getClass()}
        variant="contained"
        onClick={() => handleTokenClick(token)}
      >
        {token.text}
      </span>
    </>
  );
};

export default Token;
