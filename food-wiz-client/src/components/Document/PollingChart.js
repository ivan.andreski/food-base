import React from "react";

const PollingChart = ({ tag, datasetTags, handleAdd, handleVoteRemove }) => {
  const sourcesNum = tag.source.split(",").filter((s) => s.length > 0).length;
  const removedByNum = tag.removedBy
    .split(",")
    .filter((s) => s.length > 0).length;
  const max = sourcesNum + removedByNum;

  const handlePositiveClick = () => {
    let tagId = null;
    Object.values(datasetTags).forEach((dataset) => {
      dataset.forEach((datasetTag) => {
        if (datasetTag.tagId === tag.link) {
          tagId = datasetTag.id;
          return;
        }
      });
    });

    handleAdd(tagId);
  };

  const handleNegativeClick = () => {
    handleVoteRemove(tag.link);
  };

  return (
    <div>
      <div className="progress">
        <div
          className="progress-bar bg-success"
          role="progressbar"
          style={{ width: `${(sourcesNum / max) * 100}%` }}
          aria-valuenow={sourcesNum}
          aria-valuemin="0"
          aria-valuemax={max}
        ></div>
      </div>
      <p>Positive: {sourcesNum}</p>
      <div className="progress">
        <div
          className="progress-bar bg-danger"
          role="progressbar"
          style={{ width: `${(removedByNum / max) * 100}%` }}
          aria-valuenow={removedByNum}
          aria-valuemin="0"
          aria-valuemax={max}
        ></div>
      </div>
      <p>Negative: {removedByNum}</p>
      <div className="row p-2">
        <div className="col-md-6">
          <button
            className="btn btn-success w-100 p-1"
            onClick={handlePositiveClick}
          >
            Positive
          </button>
        </div>
        <div className="col-md-6">
          <button
            className="btn btn-danger w-100 p-1"
            onClick={handleNegativeClick}
          >
            Negative
          </button>
        </div>
      </div>
    </div>
  );
};

export default PollingChart;
