import React, {useEffect} from 'react'
import axios from '../../custom-axios/axios';
import {useNavigate} from 'react-router-dom';

const Hesoyam = () => {

const navigate = useNavigate();

useEffect(() => {
    doHesoyam();
}, []);

const doHesoyam = async () => {
    await axios.get('/corpus/reset-hard/hesoyam')
    navigate('/');
}

  return (
    <div>
      
    </div>
  )
}

export default Hesoyam
