import React, { useState } from "react";
import UploadCardJSON from "./UploadCardJSON";
import UploadCardRDF from "./UploadCardRDF";
import LoadingAnimation from "../LoadingAnimation/LoadingAnimation";
import Swal from "sweetalert2";

const Upload = () => {
  const [isLoading, setLoading] = useState(false);

  const displayResponse = (response) => {
    if (response.status === 200) {
      Swal.fire({
        icon: "success",
        title: "Success",
        html:
          '<pre style="text-align: left; white-space: pre-line;">' +
          response.message +
          "</pre>",
      });
    } else if (response.status === 500) {
      Swal.fire({
        icon: "error",
        title: "Something went wrong!",
        text: response.message,
      });
    }
  };

  return isLoading ? (
    <LoadingAnimation />
  ) : (
    <div className="container">
      <div className="row justify-content-around mb-3 mt-3 text-center">
        <div className="alert alert-warning col-5 mt-1 mb-0">
          Import datasets first, to successfully map relationships with the
          corpus!
        </div>
        <div className="alert alert-warning col-5 mt-1 mb-0">
          Attempting to upload an already existing dataset, results in an error!
        </div>
      </div>
      <div className="row justify-content-around mb-3 text-center">
        <div className="alert alert-warning col-8 mt-1 mb-0">
          Attempting to upload a corpus file with an already existing name,
          results in an error!
        </div>
      </div>

      <div className="row">
        <h3>JSON UPLOAD</h3>
      </div>
      <div className="row justify-content-left">
        <div className="col-md-5">
          <UploadCardJSON
            title="Dataset"
            changeLoading={(isLoading) => setLoading(isLoading)}
            displayResponse={(response) => displayResponse(response)}
          />
        </div>
        <div className="col-md-5 mb-3">
          <UploadCardJSON
            title="Corpus"
            changeLoading={(isLoading) => setLoading(isLoading)}
            displayResponse={(response) => displayResponse(response)}
          />
        </div>
      </div>
      <div className="row mt-3 ">
        <h3>RDF UPLOAD</h3>
      </div>
      <div className="row mb-3 ">
        <div className="col-md-5">
        <UploadCardRDF
          changeLoading={(isLoading) => setLoading(isLoading)}
          displayResponse={(response) => displayResponse(response)}
        />
        </div>
      </div>
    </div>
  );
};

export default Upload;
