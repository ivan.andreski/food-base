import React from "react";
// import CircularProgress from "@mui/material/CircularProgress"
const LoadingAnimation = () => {
  return (
    // <CircularProgress/>
    <div className="spinner-border text-primary" role="status">
      <span className="sr-only"></span>
    </div>
  );
};

export default LoadingAnimation;
