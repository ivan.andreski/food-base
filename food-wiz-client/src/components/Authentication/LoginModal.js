import React, { useState } from "react";
import Modal from "react-bootstrap/Modal";
import AuthenticationService from "../../repository/AuthenticationService";

const LoginModal = ({ setShowLogin, setAuthenticated }) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();

    AuthenticationService.login(email, password)
      .then((response) => {
        const { jwt_token } = response.data;

        localStorage.setItem("token", jwt_token);

        setAuthenticated(true);
        setEmail(email);
        AuthenticationService.setAuthToken(jwt_token);

        setShowLogin(false);
      })
      .catch((error) => setError(error.response.data.message));
  };

  return (
    <>
      <Modal.Header closeButton>
        <Modal.Title>Log in</Modal.Title>
      </Modal.Header>
      <form onSubmit={handleSubmit}>
        <Modal.Body>
          <div className="form-group w-100">
            <span>Email:</span>
            <input
              type="email"
              className="form-control"
              value={email}
              placeholder={"Email"}
              onChange={(e) => setEmail(e.target.value)}
            />
          </div>
          <div className="form-group w-100">
            <span>Password:</span>
            <input
              type="password"
              className="form-control"
              value={password}
              placeholder={"Password"}
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>
          <div className="form-group w-100">
            <span className="text-danger">{error}</span>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <button className="btn btn-success w-100">Log in</button>
        </Modal.Footer>
      </form>
    </>
  );
};

export default LoginModal;
