import React, { useState } from "react";
import Modal from "react-bootstrap/Modal";
import AuthenticationService from "../../repository/AuthenticationService";

import Validators from "./Validators";

const SignUpModal = ({ setAuthenticated, setShowSignup }) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [fullName, setFullName] = useState("");

  const [emailValid, setEmailValid] = useState({
    class: "form-control w-100",
    message: "",
    messageClass: "valid-feedback",
  });

  const [passwordValid, setPasswordValid] = useState({
    class: "form-control w-100",
    message: "",
    messageClass: "valid-feedback",
  });

  const handleSubmit = (e) => {
    e.preventDefault();

    let eValid = emailValidation();
    let pValid = passwordValidation();

    if (eValid && pValid) {
      AuthenticationService.signup(email, password, fullName)
        .then((response) => {
          AuthenticationService.login(email, password).then((resp) => {
            const { jwt_token } = response.data;

            localStorage.setItem("token", jwt_token);

            setAuthenticated(true);
            setEmail(email);
            AuthenticationService.setAuthToken(jwt_token);

            setShowSignup(false);
          });
        })
        .catch((error) => console.log(error));
    }
  };

  const emailValidation = () => {
    const result = Validators.emailValid(email);
    if (result.valid) {
      setEmailValid({
        class: "form-control w-100 is-valid",
        message: "",
        messageClass: "valid-feedback",
      });
      return true;
    } else {
      setEmailValid({
        class: "form-control w-100 is-invalid",
        message: result.message,
        messageClass: "invalid-feedback",
      });
      return false;
    }
  };

  const passwordValidation = () => {
    const result = Validators.passwordValid(password, confirmPassword);
    if (result.valid) {
      setPasswordValid({
        class: "form-control w-100 is-valid",
        message: "",
        messageClass: "valid-feedback",
      });
      return true;
    } else {
      setPasswordValid({
        class: "form-control w-100 is-invalid",
        message: result.message,
        messageClass: "invalid-feedback",
      });
      return false;
    }
  };

  return (
    <>
      <Modal.Header closeButton>
        <Modal.Title>Log in</Modal.Title>
      </Modal.Header>
      <form onSubmit={handleSubmit}>
        <Modal.Body>
          <div className="form-group w-100">
            <span>Email:</span>
            <input
              type="email"
              className={emailValid.class}
              value={email}
              placeholder={"Email"}
              onChange={(e) => setEmail(e.target.value)}
            />
            <div className={emailValid.messageClass}>{email.message}</div>
          </div>
          <div className="form-group w-100">
            <span>Full Name:</span>
            <input
              type="text"
              className="form-control"
              value={fullName}
              placeholder={"Full Name"}
              onChange={(e) => setFullName(e.target.value)}
            />
          </div>
          <div className="form-group w-100">
            <span>Password:</span>
            <input
              type="password"
              className={passwordValid.class}
              value={password}
              placeholder={"Password"}
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>
          <div className="form-group w-100">
            <span>Confirm Password:</span>
            <input
              type="password"
              className={passwordValid.class}
              value={confirmPassword}
              placeholder={"Confirm Password"}
              onChange={(e) => setConfirmPassword(e.target.value)}
            />
            <div className={password.messageClass}>{password.message}</div>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <button className="btn btn-success w-100">Sign Up</button>
        </Modal.Footer>
      </form>
    </>
  );
};

export default SignUpModal;
