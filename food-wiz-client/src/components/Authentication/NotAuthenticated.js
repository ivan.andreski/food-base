import React, { useState } from "react";
import Modal from "react-bootstrap/Modal";
import LoginModal from "./LoginModal";
import SignUpModal from "./SignUpModal";

const NotAuthenticated = ({ setAuthenticated }) => {
  const [showLogin, setShowLogin] = useState(false);
  const [showSignUp, setShowSignup] = useState(false);

  return (
    <>
      <button
        id="login"
        className="btn btn-primary"
        onClick={() => setShowLogin(true)}
      >
        Log in
      </button>

      <button
        id="signup"
        className="btn btn-secondary ml-2"
        onClick={() => setShowSignup(true)}
      >
        Sign up
      </button>

      <Modal show={showLogin} onHide={() => setShowLogin(false)}>
        <LoginModal
          setShowLogin={setShowLogin}
          setAuthenticated={setAuthenticated}
        />
      </Modal>

      <Modal show={showSignUp} onHide={() => setShowSignup(false)}>
        <SignUpModal
          setShowSignup={setShowSignup}
          setAuthenticated={setAuthenticated}
        />
      </Modal>
    </>
  );
};

export default NotAuthenticated;
