import React, { useState, useEffect } from "react";

import AuthenticationService from "../../repository/AuthenticationService";
import NotAuthenticated from "./NotAuthenticated";

const Authentication = () => {
  const [authenticated, setAuthenticated] = useState(false);
  const [email, setEmail] = useState("");
  const token = localStorage.getItem("token");

  useEffect(() => {
    AuthenticationService.setAuthToken(token);
    if (token !== null) {
      AuthenticationService.getUserEmail()
        .then((response) => {
          setEmail(response.data);
          setAuthenticated(true);
        })
        .catch((e) => console.log(e));
    }
  });

  const handleLogout = () => {
    localStorage.clear();
    setAuthenticated(false);
    setEmail("");
  };

  return authenticated ? (
    <>
      <span className="text-light mr-2">Welcome: {email}</span>
      <button className="btn btn-danger ml-2 pl-2" onClick={handleLogout}>
        Log out
      </button>
    </>
  ) : (
    <>
      <NotAuthenticated setAuthenticated={setAuthenticated}/>
    </>
  );
};

export default Authentication;
